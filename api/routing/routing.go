package routing

type Handler interface {
	HandleRoutes()
}
