package mailing

import "github.com/mailgun/mailgun-go/v4"

type Mailer interface {
	SendPasswordResetEmail(recipient, resetToken string) error
}

func NewMailer(domain, sender, privateKey string) Mailer {
	client := mailgun.NewMailgun(domain, privateKey)

	return &mailer{sender: sender, client: client}
}
