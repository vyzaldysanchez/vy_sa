package mailing

import (
	"context"
	"fmt"
	"github.com/mailgun/mailgun-go/v4"
	"net/url"
	"os"
	"time"
)

const resetTextTmpl = `
	Hi!

	A password reset request was made on your behalf.
	If this was you, please follow the link below to update your password:

	%s

	If you didn't request the password reset, please safely ignore this email. Your account will not be changed.

	Best regards,

	People at Challenge
`

const resetHTMLTmpl = `
	Hi!<br/>
	<br/>

	A password reset request was made on your behalf.
	If this was you, please follow the link below to update your password:
	<br/>

	<a href="%s">%s</a>
	
	<br/>
	If you didn't request the password reset, please safely ignore this email. Your account will not be changed.
	<br/>

	Best regards,<br/>

	People at Challenge<br/>
`

const resetPasswordSubject = "Reset your password!"

type mailer struct {
	sender string
	client *mailgun.MailgunImpl
}

func (m *mailer) SendPasswordResetEmail(recipient, resetToken string) error {
	resetBaseURL := fmt.Sprintf("%s/reset-password", os.Getenv("APP_DOMAIN"))

	v := url.Values{}

	v.Set("token", resetToken)

	resetUrl := fmt.Sprintf("%s?%s", resetBaseURL, v.Encode())
	textContent := fmt.Sprintf(resetTextTmpl, resetUrl)
	htmlContent := fmt.Sprintf(resetHTMLTmpl, resetUrl, resetUrl)

	message := m.client.NewMessage(m.sender, resetPasswordSubject, textContent, recipient)

	message.SetHtml(htmlContent)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)

	defer cancel()

	_, _, err := m.client.Send(ctx, message)

	return err
}
