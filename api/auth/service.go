package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type authService struct {
	users     users.Database
	tokens    users.TokensDatabase
	jwtSecret string
}

func (service *authService) EmailIsInUse(email string) (bool, error) {
	user, err := service.users.GetOneByEmail(email)

	if err != nil {
		return false, err
	}

	return user != nil, nil
}

func (service *authService) Logout(userId uint) error {
	return service.tokens.ExpireTokens(userId)
}

func (service *authService) Signup(email, password string) (*LoginResult, error) {
	user, err := service.users.CreateUser(email, password)

	if err != nil {
		return nil, err
	}

	authToken, err := service.generateAuthToken(user.ID)

	if err != nil {
		return nil, err
	}

	err = service.tokens.AddToken(authToken, user.ID)

	if err != nil {
		return nil, err
	}

	return &LoginResult{
		User:  user,
		Token: authToken,
	}, nil
}

type LoginResult struct {
	Token string      `json:"token"`
	User  *users.User `json:"user"`
}

func (service *authService) Login(email, password string) (*LoginResult, error) {
	user, err := service.users.GetOneByEmail(email)

	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, fmt.Errorf("no user found for: %s", email)
	}

	// We won't validate the password if it's an google OAuth signup
	if password != "" {
		passwordIsInvalidError := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

		if passwordIsInvalidError != nil {
			return nil, fmt.Errorf("password is incorrect")
		}
	}

	authToken, err := service.generateAuthToken(user.ID)

	if err != nil {
		return nil, err
	}

	err = service.tokens.AddToken(authToken, user.ID)

	if err != nil {
		return nil, err
	}

	return &LoginResult{
		User:  user,
		Token: authToken,
	}, nil
}

func (service *authService) generateAuthToken(userId uint) (string, error) {
	tokenPayload := jwt.MapClaims{
		"authorized": true,
		"userId":     userId,
		"expiry":     time.Now().Add(time.Hour * 24 * 7).Unix(),
	}

	jwtToken := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tokenPayload)

	token, err := jwtToken.SignedString([]byte(service.jwtSecret))

	if err != nil {
		return "", fmt.Errorf("something went wrong when signing auth token: %v", err)
	}

	return token, nil
}

type Service interface {
	Logout(userId uint) error
	EmailIsInUse(email string) (bool, error)
	Login(email, password string) (*LoginResult, error)
	Signup(email, password string) (*LoginResult, error)
}

func NewService(users users.Database, tokens users.TokensDatabase, jwtSecret string) Service {
	return &authService{users: users, tokens: tokens, jwtSecret: jwtSecret}
}
