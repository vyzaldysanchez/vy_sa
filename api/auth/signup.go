package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"net/http"
	"strings"
)

type SignupRequest struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"passwordConfirmation"`
}

func (h *handler) signup(writer http.ResponseWriter, request *http.Request) error {
	signupPayload := new(SignupRequest)

	err := utils.ParseJSON(request, signupPayload)

	if err != nil {
		return err
	}

	if signupPayload.Email == "" {
		http.Error(writer, fmt.Sprint("email is missing"), http.StatusBadRequest)

		return nil
	}

	if signupPayload.Password == "" {
		http.Error(writer, fmt.Sprint("password is missing"), http.StatusBadRequest)

		return nil
	}

	if signupPayload.Password != signupPayload.PasswordConfirmation {
		http.Error(writer, fmt.Sprint("password does not match confirmation"), http.StatusBadRequest)

		return nil
	}

	signupPayload.Email = strings.ToLower(signupPayload.Email)
	signupPayload.Email = strings.TrimSpace(signupPayload.Email)

	if !utils.IsEmailValid(signupPayload.Email) {
		http.Error(writer, fmt.Sprint("email has invalid format"), http.StatusBadRequest)

		return nil
	}

	emailIsInUse, err := h.service.EmailIsInUse(signupPayload.Email)

	if emailIsInUse {
		http.Error(writer, fmt.Sprint("email is already in use"), http.StatusBadRequest)

		return nil
	}

	loginResult, err := h.service.Signup(signupPayload.Email, signupPayload.Password)

	if err != nil {
		return err
	}

	err = utils.SendJSON(writer, loginResult)

	if err != nil {
		return err
	}

	return nil
}
