package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/routing"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"github.com/gorilla/mux"
)

type handler struct {
	service Service
	router  *mux.Router
}

func (h *handler) HandleRoutes() {
	router := h.router.PathPrefix("/auth").Subrouter()

	router.HandleFunc(
		"/login",
		utils.HandleInternalServerError(h.login),
	).Methods("POST")
	router.HandleFunc(
		"/signup",
		utils.HandleInternalServerError(h.signup),
	).Methods("POST")
	router.HandleFunc(
		"/logout",
		utils.HandleInternalServerError(h.logout),
	).Methods("POST")

	oauthRouter := router.PathPrefix("/google").Subrouter()

	oauthLogin := utils.HandleInternalServerError(h.OAuthLogin)

	oauthRouter.HandleFunc("/login", oauthLogin).Methods("POST")
	oauthRouter.HandleFunc("/signup", oauthLogin).Methods("POST")
	oauthRouter.HandleFunc(
		"/callback",
		utils.HandleInternalServerError(h.OAuthCallback),
	).Methods("GET")
}

func NewHandler(service Service, router *mux.Router) routing.Handler {
	return &handler{
		service: service,
		router:  router,
	}
}
