package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"net/http"
)

func (h *handler) logout(writer http.ResponseWriter, request *http.Request) error {
	user := users.GetUser(request.Context())

	if user != nil {
		err := h.service.Logout(user.ID)

		if err != nil {
			return err
		}
	}

	utils.SendOK(writer)

	return nil
}
