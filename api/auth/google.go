package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/config"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type OAuthUser struct {
	// Sub           string `json:"sub"`
	// Name          string `json:"name"`
	Email string `json:"email"`
	// Gender        string `json:"gender"`
	// Profile       string `json:"profile"`
	// Picture       string `json:"picture"`
	// GivenName     string `json:"given_name"`
	// FamilyName    string `json:"family_name"`
	// EmailVerified bool   `json:"email_verified"`
}

type OAuthResult struct {
	OAuthUrl string `json:"oauthUrl"`
}

func (h *handler) OAuthLogin(writer http.ResponseWriter, _ *http.Request) error {
	oauthState := generateStateOauthCookie(writer)

	url := config.GetGoogleOAuthConfig().AuthCodeURL(oauthState)

	fmt.Println(url)

	return utils.SendJSON(writer, OAuthResult{OAuthUrl: url})
}

func generateStateOauthCookie(writer http.ResponseWriter) string {
	expiration := time.Now().Add(time.Hour * 24 * 7)

	bytes := make([]byte, 16)

	rand.Read(bytes)

	state := base64.URLEncoding.EncodeToString(bytes)

	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}

	http.SetCookie(writer, &cookie)

	return state
}

func (h *handler) OAuthCallback(writer http.ResponseWriter, request *http.Request) error {
	oauthState, _ := request.Cookie("oauthstate")

	if oauthState == nil {
		http.Error(writer, "oauth state not available", http.StatusBadRequest)
		return nil
	}

	if request.FormValue("state") != oauthState.Value {
		http.Error(writer, "oauth state value doesn't match", http.StatusBadRequest)

		return nil
	}

	data, err := getUserDataFromGoogle(request.FormValue("code"))

	if err != nil {
		return err
	}

	userData := new(OAuthUser)

	if err = json.Unmarshal(data, &userData); err != nil {
		return err
	}

	emailIsInUse, err := h.service.EmailIsInUse(userData.Email)

	if err != nil {
		return err
	}

	var loginResult *LoginResult

	if emailIsInUse {
		loginResult, err = h.service.Login(userData.Email, "")
	} else {
		loginResult, err = h.service.Signup(userData.Email, "")
	}

	if err != nil {
		return err
	}

	utils.SendJSON(writer, loginResult)

	return nil
}

func getUserDataFromGoogle(code string) ([]byte, error) {
	token, err := config.GetGoogleOAuthConfig().Exchange(context.Background(), code)

	if err != nil {
		return nil, fmt.Errorf("failed converting authorization code into a token: %s", err.Error())
	}

	response, err := http.Get(os.Getenv("GOOGLE_OAUTH_URL_API") + token.AccessToken)

	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}

	defer response.Body.Close()

	contents, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}

	return contents, nil
}
