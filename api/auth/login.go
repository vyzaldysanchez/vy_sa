package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"errors"
	"fmt"
	"net/http"
)

type LoginCredentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (h *handler) login(writer http.ResponseWriter, request *http.Request) error {
	credentials := new(LoginCredentials)

	err := utils.ParseJSON(request, &credentials)

	if err != nil {
		return errors.New("error parsing body to JSON")
	}

	if credentials.Email == "" {
		http.Error(writer, fmt.Sprint("email is missing"), http.StatusBadRequest)

		return nil
	}

	if credentials.Password == "" {
		http.Error(writer, fmt.Sprint("password is missing"), http.StatusBadRequest)

		return nil
	}

	loginResult, err := h.service.Login(credentials.Email, credentials.Password)

	if err != nil {
		return err
	}

	err = utils.SendJSON(writer, loginResult)

	if err != nil {
		return err
	}

	return nil
}
