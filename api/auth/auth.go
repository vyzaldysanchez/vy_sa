package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"github.com/gorilla/mux"
	"os"
)

func StartModule(users users.Database, tokens users.TokensDatabase, router *mux.Router) {
	authService := NewService(users, tokens, os.Getenv("JWT_SECRET"))

	NewHandler(authService, router).HandleRoutes()
}
