package utils

import (
	"encoding/json"
	"net/http"
)

func ParseJSON(request *http.Request, destination interface{}) error {
	defer request.Body.Close()

	return json.NewDecoder(request.Body).Decode(&destination)
}

func SendJSON(writer http.ResponseWriter, body interface{}) error {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)

	return json.NewEncoder(writer).Encode(body)
}

func SendJSONBadRequest(writer http.ResponseWriter, payload interface{}) error {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusBadRequest)

	return json.NewEncoder(writer).Encode(payload)
}

func SendOK(writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
}

type routerHandlerWithError = func(writer http.ResponseWriter, request *http.Request) error

func HandleInternalServerError(handler routerHandlerWithError) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		err := handler(writer, request)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusForbidden)
			return
		}
	}
}
