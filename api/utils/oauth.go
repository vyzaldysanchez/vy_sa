package utils

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"strconv"
)

func GetUserId(request *http.Request) (uint, string, error) {
	token := getToken(request)

	parsedToken, err := parseToken(token)

	if err != nil {
		return 0, "", fmt.Errorf("failed parsing oauth token: %v", err.Error())
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)

	if ok && parsedToken.Valid {
		id, err := strconv.ParseUint(fmt.Sprintf("%.0f", claims["userId"]), 10, 32)

		if err != nil {
			return 0, "", fmt.Errorf("failed parsing oauth token: %v", err.Error())
		}

		return uint(id), token, nil
	}

	return 0, "", nil
}

func getToken(request *http.Request) string {
	queryParams := request.URL.Query()

	token := queryParams.Get("token")

	if token != "" {
		return token
	}

	token = request.Header.Get("Authorization")

	//if tokenParts := strings.Split(token, "."); len(tokenParts) == 3 {
	if token != "" {
		return token
	}

	return ""
}

func parseToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("invalid signing method -> %v", token.Header["alg"])
		}

		return []byte(os.Getenv("JWT_SECRET")), nil
	})
}
