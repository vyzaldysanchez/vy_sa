package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"hash"
)

type HMAC struct {
	hmac hash.Hash
}

func (h HMAC) Hash(value string) string {
	h.hmac.Reset()
	h.hmac.Write([]byte(value))

	bytes := h.hmac.Sum(nil)

	return base64.URLEncoding.EncodeToString(bytes)
}

func NewHMAC(hmacKey string) HMAC {
	hmacHash := hmac.New(sha256.New, []byte(hmacKey))

	return HMAC{hmac: hmacHash}
}
