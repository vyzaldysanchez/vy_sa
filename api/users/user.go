package users

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model

	Email           string `gorm:"not null,unique" json:"email"`
	Password        string `json:"-"`
	Address         string `json:"address"`
	FullName        string `json:"fullName"`
	Telephone       string `json:"telephone"`
	IsGoogleAccount bool   `json:"isGoogleAccount"`

	Tokens []OAuthToken `gorm:"foreignkey:UserId;association_foreignkey:ID" json:"-"`
}

type users struct {
	db *gorm.DB
}

type Database interface {
	GetOneById(id uint) (*User, error)
	GetOneByEmail(email string) (*User, error)

	UpdateUser(userId uint, user *User) error
	CreateUser(email, password string) (*User, error)
}

func (u *users) GetOneById(id uint) (*User, error) {
	if id == 0 {
		return nil, nil
	}

	user := new(User)

	result := u.db.Preload("Tokens", "is_expired = ?", false).Where("id = ?", id).First(&user)

	if result.RecordNotFound() {
		return nil, nil
	}

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("something went wrong when fetching the user: %v", err.Error())
	}

	return user, nil
}

func (u *users) GetOneByEmail(email string) (*User, error) {
	user := new(User)

	result := u.db.Model(&User{}).Where("email = ?", email).First(&user)

	if result.RecordNotFound() {
		return nil, nil
	}

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("something went wrong when fetching the user: %v", err.Error())
	}

	return user, nil
}

func (u *users) CreateUser(email, password string) (*User, error) {
	user := new(User)

	user.IsGoogleAccount = password == ""

	// We won't set the password if it's an google OAuth signup
	if !user.IsGoogleAccount {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

		if err != nil {
			return nil, fmt.Errorf("something went wrong when hashing password: %v", err.Error())
		}

		user.Password = string(passwordHash)
	}

	user.Email = email

	result := u.db.Create(user).Scan(&user)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("something went wrong when creating the user: %v", err.Error())
	}

	return user, nil
}

func (u *users) UpdateUser(userId uint, user *User) error {
	if user.Password != "" {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

		user.Password = string(passwordHash)

		if err != nil {
			return fmt.Errorf("something went wrong when hashing password: %v", err.Error())
		}
	}

	result := u.db.Model(&User{}).Where("id = ?", userId).Update(user)

	if err := result.Error; err != nil {
		return fmt.Errorf("something went wrong when updating the user: %v", err.Error())
	}

	return nil
}

func NewDatabaseService(db *gorm.DB) Database {
	db.AutoMigrate(&User{})

	return &users{db: db}
}
