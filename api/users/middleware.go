package users

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func UserRequired(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		user := GetUser(request.Context())

		if user != nil {
			handler.ServeHTTP(writer, request)
			return
		}

		http.Error(writer, fmt.Sprintf("cannot access this route"), http.StatusForbidden)
	})
}

func UserOwnsRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		userId, err := strconv.ParseUint(vars["userId"], 10, 32)

		if err != nil {
			http.Error(
				writer,
				fmt.Sprintf("couldn't parse userId from params: %v", err.Error()),
				http.StatusInternalServerError,
			)
			return
		}

		user := GetUser(request.Context())

		if uint(userId) == user.ID {
			handler.ServeHTTP(writer, request)
			return
		}

		http.Error(writer, fmt.Sprintf("cannot access this route"), http.StatusForbidden)
	})
}

func MakeLoadUser(users Database) func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return loadUser(users, handler.ServeHTTP)
	}
}

func loadUser(users Database, handler http.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		userId, token, _ := utils.GetUserId(request)

		user, err := users.GetOneById(userId)

		if user == nil {
			handler.ServeHTTP(writer, request)
			return
		}

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		var tokenFound bool

		for _, userToken := range user.Tokens {
			if userToken.Token == token && !userToken.IsExpired {
				tokenFound = true
				break
			}
		}

		if tokenFound {
			ctx := WithUser(request.Context(), user)

			request = request.WithContext(ctx)
		}

		handler.ServeHTTP(writer, request)
	})
}
