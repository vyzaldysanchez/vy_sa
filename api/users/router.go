package users

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/routing"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"github.com/gorilla/mux"
)

type handler struct {
	db     Database
	router *mux.Router
}

func (h *handler) HandleRoutes() {
	usersRouter := h.router.PathPrefix("/users").Subrouter()

	usersRouter.Use(UserRequired)
	usersRouter.Use(UserOwnsRequest)

	usersRouter.HandleFunc(
		"/{userId}",
		utils.HandleInternalServerError(h.GetUser),
	).Methods("GET")
	usersRouter.HandleFunc(
		"/{userId}",
		utils.HandleInternalServerError(h.UpdateUser),
	).Methods("POST")
}

func NewHandler(db Database, r *mux.Router) routing.Handler {
	return &handler{db: db, router: r}
}
