package users

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"net/http"
)

func (h *handler) GetUser(writer http.ResponseWriter, request *http.Request) error {
	user := GetUser(request.Context())

	err := utils.SendJSON(writer, user)

	if err != nil {
		return fmt.Errorf("failed to parse user data: %v", err.Error())
	}

	return nil
}
