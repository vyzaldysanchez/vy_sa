package users

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
)

func addressIsRequired(user *User) error {
	if user.Address == "" {
		return fmt.Errorf("address is required")
	}

	return nil
}

func fullNameIsRequired(user *User) error {
	if user.Address == "" {
		return fmt.Errorf("fullName is required")
	}

	return nil
}

func telephoneIsRequired(user *User) error {
	if user.Telephone == "" {
		return fmt.Errorf("telephone is required")
	}

	return nil
}

func emailIsValid(user *User) error {
	if !utils.IsEmailValid(user.Email) {
		return fmt.Errorf("email is not valid")
	}

	return nil
}

type validation = func(user *User) error

func runValidations(user *User, validations ...validation) []string {
	errors := make([]string, len(validations))

	for _, validation := range validations {
		if err := validation(user); err != nil {
			errors = append(errors, err.Error())
		}
	}

	return nil
}
