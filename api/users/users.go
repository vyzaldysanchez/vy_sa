package users

import (
	"github.com/gorilla/mux"
)

func StartModule(db Database, r *mux.Router) {
	NewHandler(db, r).HandleRoutes()
}
