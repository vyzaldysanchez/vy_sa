package users

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

type OAuthToken struct {
	gorm.Model

	Token     string `gorm:"unique;not null" json:"token"`
	UserId    uint   `gorm:"not null" json:"userId"`
	IsExpired bool   `json:"isExpired"`
}

type oauthTokens struct {
	db *gorm.DB
}

func (t *oauthTokens) AddToken(token string, userId uint) error {
	result := t.db.Create(&OAuthToken{
		Token:     token,
		UserId:    userId,
		IsExpired: false,
	})

	if err := result.Error; err != nil {
		return fmt.Errorf("something went wrong when creating the token: %v", err.Error())
	}

	return nil
}

func (t *oauthTokens) ExpireTokens(userId uint) error {
	result := t.db.Model(&OAuthToken{}).Where(&OAuthToken{UserId: userId}).Update(&OAuthToken{IsExpired: true})

	if err := result.Error; err != nil {
		return fmt.Errorf("something went wrong when trying to expire token: %v", err.Error())
	}

	return nil
}

type TokensDatabase interface {
	ExpireTokens(userId uint) error
	AddToken(token string, userId uint) error
}

func NewOAuthTokensDatabase(db *gorm.DB) TokensDatabase {
	db.AutoMigrate(&OAuthToken{})
	db.Model(&OAuthToken{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")

	return &oauthTokens{db: db}
}
