package users

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"net/http"
)

func (h *handler) UpdateUser(writer http.ResponseWriter, request *http.Request) error {
	currentUser := GetUser(request.Context())

	fieldsToUpdate := &User{}

	err := utils.ParseJSON(request, fieldsToUpdate)

	if err != nil {
		return fmt.Errorf("couldn't parse JSON data: %v", err.Error())
	}

	if currentUser.IsGoogleAccount {
		fieldsToUpdate.Email = currentUser.Email
	}

	validationErrors := runValidations(
		fieldsToUpdate,
		emailIsValid,
	)

	if len(validationErrors) == 0 {
		err := h.db.UpdateUser(currentUser.ID, fieldsToUpdate)

		if err != nil {
			return fmt.Errorf("couldn't update the user: %v", err.Error())
		}

		utils.SendOK(writer)

		return nil
	}

	return utils.SendJSONBadRequest(writer, validationErrors)
}
