package users

import (
	"context"
)

const userContextKey string = "user-context"

func WithUser(ctx context.Context, user *User) context.Context {
	return context.WithValue(ctx, userContextKey, user)
}

func GetUser(ctx context.Context) *User {
	userInContext := ctx.Value(userContextKey)

	if userInContext != nil {
		if user, ok := userInContext.(*User); ok {
			return user
		}
	}

	return nil
}
