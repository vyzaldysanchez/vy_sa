package database

import (
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"os"
	"strconv"
)

func ConnectDB() (*gorm.DB, error) {
	host := os.Getenv("DB_HOST")

	if os.Getenv("DB_PORT") != "" {
		port, err := strconv.Atoi(os.Getenv("DB_PORT"))

		if err != nil {
			return nil, errors.New("could not parse DB port")
		}

		host = fmt.Sprintf("tcp(%s:%d)", host, port)
	}

	dbURL := fmt.Sprintf(
		"%s:%s@%s/%s?charset=utf8&parseTime=true&loc=Local",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		host,
		os.Getenv("DB_NAME"),
	)

	database, err := gorm.Open("mysql", dbURL)

	if err != nil {
		return nil, fmt.Errorf("couldn't open database: %v", err.Error())
	}

	return database, nil
}
