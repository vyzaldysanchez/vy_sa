package passwords

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/mailing"
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"github.com/gorilla/mux"
)

func StartModule(db Database, u users.Database, mailer mailing.Mailer, r *mux.Router) {
	NewHandler(db, u, mailer, r).HandleRoutes()
}
