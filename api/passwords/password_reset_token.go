package passwords

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/jinzhu/gorm"
)

type PasswordResetToken struct {
	gorm.Model

	UserID    uint   `gorm:"not null"`
	Token     string `gorm:"-"`
	TokenHash string `gorm:"not null;unique"`
}

type passwordResets struct {
	db   *gorm.DB
	hmac utils.HMAC
}

func (p *passwordResets) Delete(id uint) error {
	return p.db.Model(&PasswordResetToken{}).Where("id = ?", id).Delete(PasswordResetToken{}).Error
}

func (p *passwordResets) CreateForUser(userId uint) (*PasswordResetToken, error) {
	token, err := getRememberToken(32)

	if err != nil {
		return nil, fmt.Errorf("someting went wrong when generating password reset token: %v", err.Error())
	}

	passwordResetToken := &PasswordResetToken{
		UserID:    userId,
		Token:     token,
		TokenHash: p.hmac.Hash(token),
	}

	err = p.db.Create(passwordResetToken).Error

	if err != nil {
		return nil, fmt.Errorf("someting went wrong when generating password reset token: %v", err.Error())
	}

	return passwordResetToken, nil
}

func (p *passwordResets) GetByUserId(userId uint) (*PasswordResetToken, error) {
	passwordResetToken := &PasswordResetToken{}

	result := p.db.Model(&PasswordResetToken{}).Where("user_id = ?", userId).First(passwordResetToken)

	if result.RecordNotFound() {
		return nil, nil
	}

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("something went wrong when fetching password token: %v", err.Error())
	}

	return passwordResetToken, nil
}

func (p *passwordResets) GetByToken(token string) (*PasswordResetToken, error) {
	passwordResetToken := &PasswordResetToken{}

	tokenHash := p.getHmacFromToken(token)

	result := p.db.Model(&PasswordResetToken{}).Where("token_hash = ?", tokenHash).First(passwordResetToken)

	if result.RecordNotFound() {
		return nil, nil
	}

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("something went wrong when fetching password token: %v", err.Error())
	}

	return passwordResetToken, nil
}

func (p *passwordResets) getHmacFromToken(token string) string {
	if token == "" {
		return ""
	}

	return p.hmac.Hash(token)
}

func getRememberToken(numberOfBytes uint) (string, error) {
	b := make([]byte, numberOfBytes)

	_, err := rand.Read(b)

	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

type Database interface {
	Delete(id uint) error
	GetByUserId(userId uint) (*PasswordResetToken, error)
	GetByToken(token string) (*PasswordResetToken, error)
	CreateForUser(userId uint) (*PasswordResetToken, error)
}

func NewDatabase(db *gorm.DB, hmac utils.HMAC) Database {
	db.AutoMigrate(&PasswordResetToken{})

	return &passwordResets{db: db, hmac: hmac}
}
