package passwords

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"net/http"
	"time"
)

type ResetPasswordRequest struct {
	Token       string `json:"token"`
	NewPassword string `json:"newPassword"`
}

func (h *handler) resetPassword(writer http.ResponseWriter, request *http.Request) error {
	data := new(ResetPasswordRequest)

	err := utils.ParseJSON(request, data)

	if err != nil {
		return fmt.Errorf("couldn't parse JSON data: %v", err.Error())
	}

	if data.Token == "" {
		http.Error(writer, "the token received via email is required", http.StatusBadRequest)
		return nil
	}

	passwordReset, err := h.db.GetByToken(data.Token)

	if err != nil {
		return fmt.Errorf("couldn't load password reset request: %v", err.Error())
	}

	if passwordReset == nil {
		http.Error(writer, "there's no password reset request for that token", http.StatusNotFound)
		return nil
	}

	if time.Now().Sub(passwordReset.CreatedAt) > (15 * time.Minute) {
		http.Error(writer, "the password reset request token is expired", http.StatusBadRequest)
		return nil
	}

	newPassword := data.NewPassword

	if newPassword == "" {
		http.Error(writer, "newPassword is required", http.StatusBadRequest)
		return nil
	}

	user, err := h.users.GetOneById(passwordReset.UserID)

	if err != nil {
		return fmt.Errorf("couldn't fetch user: %v", err.Error())
	}

	if user == nil {
		http.Error(writer, "there's no user for that password reset request", http.StatusNotFound)
		return nil
	}

	err = h.users.UpdateUser(user.ID, &users.User{Password: newPassword})

	if err != nil {
		return fmt.Errorf("couldn't update user password: %v", err.Error())
	}

	err = h.db.Delete(passwordReset.ID)

	if err != nil {
		return fmt.Errorf("couldn't delete password reset token: %v", err.Error())
	}

	return nil
}
