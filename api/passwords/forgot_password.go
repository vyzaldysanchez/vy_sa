package passwords

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"net/http"
)

type ForgotPasswordRequest struct {
	Email string `json:"email"`
}

func (h *handler) forgotPassword(writer http.ResponseWriter, request *http.Request) error {
	req := new(ForgotPasswordRequest)

	err := utils.ParseJSON(request, req)

	if err != nil {
		return fmt.Errorf("couldn't parse JSON data: %v", err.Error())
	}

	user, err := h.users.GetOneByEmail(req.Email)

	if err != nil {
		return err
	}

	if user.Email != req.Email {
		http.Error(writer, "email does not match account's email", http.StatusBadRequest)

		return nil
	}

	currentToken, err := h.db.CreateForUser(user.ID)

	if err != nil {
		return fmt.Errorf("could create password reset token for user: %v", err.Error())
	}

	if currentToken != nil {
		err := h.db.Delete(currentToken.ID)

		if err != nil {
			return fmt.Errorf("could create password reset token for user: %v", err.Error())
		}
	}

	passwordToken, err := h.db.CreateForUser(user.ID)

	if err != nil {
		return fmt.Errorf("could create password reset token for user: %v", err.Error())
	}

	err = h.mailer.SendPasswordResetEmail(user.Email, passwordToken.Token)

	if err != nil {
		return fmt.Errorf("could send password reset email for user: %v", err.Error())
	}

	return nil
}
