package passwords

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/mailing"
	"bitbucket.org/vyzaldysanchez/user-management/api/routing"
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"github.com/gorilla/mux"
)

type handler struct {
	db     Database
	r      *mux.Router
	users  users.Database
	mailer mailing.Mailer
}

func (h *handler) HandleRoutes() {
	passwordsRouter := h.r.PathPrefix("/passwords").Subrouter()

	passwordsRouter.HandleFunc(
		"/forgot",
		utils.HandleInternalServerError(h.forgotPassword),
	).Methods("POST")

	passwordsRouter.HandleFunc(
		"/reset",
		utils.HandleInternalServerError(h.resetPassword),
	).Methods("POST")
}

func NewHandler(db Database, u users.Database, mailer mailing.Mailer, r *mux.Router) routing.Handler {
	return &handler{db: db, users: u, mailer: mailer, r: r}
}
