package api

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/auth"
	"bitbucket.org/vyzaldysanchez/user-management/api/mailing"
	"bitbucket.org/vyzaldysanchez/user-management/api/passwords"
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/api/utils"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
	"os"
)

func Start(db *gorm.DB, r *mux.Router) http.Handler {
	if os.Getenv("PRODUCTION") == "false" {
		db.LogMode(true)
	}

	apiRouter := r.PathPrefix("/api").Subrouter()

	usersDatabase := users.NewDatabaseService(db)
	tokensDatabase := users.NewOAuthTokensDatabase(db)
	passwordTokensDatabase := passwords.NewDatabase(
		db,
		utils.NewHMAC(os.Getenv("HMAC_KEY")),
	)

	credentialsOk := handlers.AllowCredentials()
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	headersOk := handlers.AllowedHeaders([]string{"Accept", "Accept-Language", "Content-Type", "Authorization", "Content-Language", "Origin"})

	cors := handlers.CORS(originsOk, headersOk, credentialsOk, methodsOk)

	apiRouter.Use(cors)
	apiRouter.Use(users.MakeLoadUser(usersDatabase))

	domain := os.Getenv("MAILGUN_DOMAIN")
	senderEmail := fmt.Sprintf("%s@%s", "support", domain)

	mailer := mailing.NewMailer(
		domain,
		senderEmail,
		os.Getenv("MAILGUN_PRIVATE_KEY"),
	)

	users.StartModule(usersDatabase, apiRouter)
	auth.StartModule(usersDatabase, tokensDatabase, apiRouter)
	passwords.StartModule(passwordTokensDatabase, usersDatabase, mailer, apiRouter)

	return handlers.LoggingHandler(os.Stdout, apiRouter)
}
