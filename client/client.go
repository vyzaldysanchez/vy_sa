package client

import (
	apiClient "bitbucket.org/vyzaldysanchez/user-management/client/api-client"
	"bitbucket.org/vyzaldysanchez/user-management/client/auth"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

func Start(r *mux.Router) http.Handler {
	client := apiClient.New(os.Getenv("APP_DOMAIN"))

	return auth.NewHandler(r, client).HandleRoutes()
}
