package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"net/http"
)

func (h *handler) forgotPassword(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderForgotPassword(writer, request)

		return
	}

	err = h.api.ForgotPassword(request.FormValue("email"))

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderForgotPassword(writer, request)

		return
	}

	session.AddFlash("an email will be sent to you with a link to continue the password reset process")

	_ = session.Save(request, writer)

	h.renderForgotPassword(writer, request)
}
