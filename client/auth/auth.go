package auth

import (
	apiClient "bitbucket.org/vyzaldysanchez/user-management/client/api-client"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

type handler struct {
	r   *mux.Router
	api *apiClient.Client
}

func (h *handler) HandleRoutes() http.Handler {
	clientRouter := h.r.PathPrefix("/").Subrouter()

	clientRouter.HandleFunc("/", authGuard(h.homeHandler)).Methods("GET")

	clientRouter.HandleFunc("/login", freeGuard(h.login)).Methods("POST")
	clientRouter.HandleFunc("/login", freeGuard(h.renderLogin)).Methods("GET")

	clientRouter.HandleFunc("/signup", freeGuard(h.signup)).Methods("POST")
	clientRouter.HandleFunc("/signup", freeGuard(h.renderSignup)).Methods("GET")

	clientRouter.HandleFunc("/auth/google", freeGuard(h.oauth)).Methods("GET")
	clientRouter.HandleFunc("/auth/google/callback", freeGuard(h.oauthCallback)).Methods("GET")

	clientRouter.HandleFunc("/logout", authGuard(h.logout)).Methods("GET")

	clientRouter.HandleFunc("/forgot-password", freeGuard(h.renderForgotPassword)).Methods("GET")
	clientRouter.HandleFunc("/forgot-password", freeGuard(h.forgotPassword)).Methods("POST")

	clientRouter.HandleFunc("/reset-password", freeGuard(h.renderResetPassword)).Methods("GET")
	clientRouter.HandleFunc("/reset-password", freeGuard(h.resetPassword)).Methods("POST")

	clientRouter.HandleFunc("/profile", authGuard(h.renderProfile)).Methods("GET", "POST")
	clientRouter.HandleFunc("/profile/edit", authGuard(h.renderEditProfile)).Methods("GET")
	clientRouter.HandleFunc("/profile/edit", authGuard(h.editProfile)).Methods("POST")

	clientRouter.HandleFunc("*", authGuard(h.homeHandler))

	return handlers.LoggingHandler(os.Stdout, clientRouter)
}

type Handler interface {
	HandleRoutes() http.Handler
}

func NewHandler(r *mux.Router, api *apiClient.Client) Handler {
	return &handler{
		r:   r,
		api: api,
	}
}
