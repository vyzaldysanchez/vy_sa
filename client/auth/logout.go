package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"fmt"
	"net/http"
)

func (h *handler) logout(writer http.ResponseWriter, request *http.Request) {
	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	token := fmt.Sprintf("%s", session.Values["token"])

	err := h.api.Logout(token)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["token"] = nil
	session.Values["userId"] = nil

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(writer, request, "/", http.StatusTemporaryRedirect)
}
