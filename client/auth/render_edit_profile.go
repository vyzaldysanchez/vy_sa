package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"html/template"
	"net/http"
	"path"
)

type profileData struct {
	User          interface{}
	FlashMessages interface{}
}

func (h *handler) renderEditProfile(writer http.ResponseWriter, request *http.Request) {
	fp := path.Join("client", "templates", "edit-profile.html")
	tmpl, err := template.ParseFiles(fp)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	flashMessages := session.Flashes()

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(writer, profileData{
		User:          session.Values["user"],
		FlashMessages: flashMessages,
	})

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}
