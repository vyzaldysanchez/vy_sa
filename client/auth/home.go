package auth

import (
	"net/http"
)

func (h *handler) homeHandler(writer http.ResponseWriter, request *http.Request) {
	http.Redirect(writer, request, "/profile", http.StatusTemporaryRedirect)
}
