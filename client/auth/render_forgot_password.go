package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"html/template"
	"net/http"
	"path"
)

type forgotPasswordData struct {
	FlashMessages interface{}
}

func (h *handler) renderForgotPassword(writer http.ResponseWriter, request *http.Request) {
	fp := path.Join("client", "templates", "forgot-password.html")
	tmpl, err := template.ParseFiles(fp)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	flashes := session.Flashes()

	session.Save(request, writer)

	err = tmpl.Execute(writer, forgotPasswordData{FlashMessages: flashes})

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}
