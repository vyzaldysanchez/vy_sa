package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"html/template"
	"net/http"
	"path"
)

type resetPasswordData struct {
	Token         string
	FlashMessages interface{}
}

func (h *handler) renderResetPassword(writer http.ResponseWriter, request *http.Request) {
	token := request.URL.Query().Get("token")

	if token == "" {
		http.Redirect(writer, request, "/login", http.StatusTemporaryRedirect)
		return
	}

	fp := path.Join("client", "templates", "reset-password.html")
	tmpl, err := template.ParseFiles(fp)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	flashes := session.Flashes()

	session.Save(request, writer)

	err = tmpl.Execute(writer, resetPasswordData{FlashMessages: flashes, Token: token})

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}
