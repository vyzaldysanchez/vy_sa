package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"net/http"
)

func (h *handler) login(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderLogin(writer, request)

		return
	}

	response, err := h.api.Login(request.FormValue("email"), request.FormValue("password"))

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderLogin(writer, request)

		return
	}

	session.Values["token"] = response.Token
	session.Values["user"] = response.User

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	request.Form.Del("email")
	request.Form.Del("password")

	http.Redirect(writer, request, "/profile", http.StatusTemporaryRedirect)
}
