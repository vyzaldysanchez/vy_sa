package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"fmt"
	"net/http"
)

func (h *handler) editProfile(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderEditProfile(writer, request)

		return
	}

	sessionUser := session.Values["user"].(users.User)
	email := sessionUser.Email

	sessionUser.Address = request.FormValue("address")
	sessionUser.FullName = request.FormValue("fullName")
	sessionUser.Telephone = request.FormValue("telephone")

	if !sessionUser.IsGoogleAccount {
		sessionUser.Email = request.FormValue("email")
	}

	token := fmt.Sprintf("%s", session.Values["token"])

	err = h.api.EditProfile(sessionUser.ID, &sessionUser, token)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderEditProfile(writer, request)

		return
	}

	sessionUser.Email = email

	session.Values["user"] = sessionUser
	session.Values["token"] = session.Values["token"]

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	request.Form.Del("email")
	request.Form.Del("address")
	request.Form.Del("fullName")
	request.Form.Del("telephone")

	http.Redirect(writer, request, "/profile", http.StatusTemporaryRedirect)
}
