package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"html/template"
	"net/http"
	"path"
)

func (h *handler) renderProfile(writer http.ResponseWriter, request *http.Request) {
	fp := path.Join("client", "templates", "profile.html")
	tmpl, err := template.ParseFiles(fp)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	err = tmpl.Execute(writer, session.Values["user"])

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}
