package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"net/http"
)

func (h *handler) resetPassword(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderResetPassword(writer, request)

		return
	}

	token := request.FormValue("token")

	err = h.api.ResetPassword(request.FormValue("password"), token)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		query := request.URL.Query()
		query.Add("token", token)

		request.URL.RawQuery = query.Encode()

		h.renderResetPassword(writer, request)

		return
	}

	http.Redirect(writer, request, "/login", http.StatusTemporaryRedirect)
}
