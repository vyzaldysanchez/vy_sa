package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"net/http"
)

func (h *handler) oauth(writer http.ResponseWriter, request *http.Request) {
	result := h.api.GetOAuthLink()

	h.api.SetCookie(request.URL, result.OAuthState)

	http.Redirect(writer, request, result.OAuthUrl, http.StatusTemporaryRedirect)
}

func (h *handler) oauthCallback(writer http.ResponseWriter, request *http.Request) {
	response, err := h.api.HandleOauthCallback(request.URL.RawQuery)

	if err != nil {
		http.Redirect(writer, request, "/login", http.StatusTemporaryRedirect)
		return
	}

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	session.Values["token"] = response.Token
	session.Values["user"] = response.User

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(writer, request, "/profile", http.StatusTemporaryRedirect)
}
