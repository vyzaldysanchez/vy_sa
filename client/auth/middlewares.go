package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"fmt"
	"net/http"
)

func authGuard(handler http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

		if session.Values["token"] == nil {
			http.Redirect(writer, request, "/login", http.StatusTemporaryRedirect)
			return
		}

		handler.ServeHTTP(writer, request)
	}
}

func freeGuard(handler http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

		if session.Values["token"] != nil {
			if request.Method == http.MethodPost || request.Method == http.MethodDelete {
				fmt.Println("token available, redirect..?")
				http.Error(writer, "cannot access this route", http.StatusInternalServerError)
				return
			}

			http.Redirect(writer, request, "/profile", http.StatusTemporaryRedirect)
			return
		}

		handler.ServeHTTP(writer, request)
	}
}
