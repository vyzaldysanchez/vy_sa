package auth

import (
	"bitbucket.org/vyzaldysanchez/user-management/client/sessions"
	"net/http"
)

func (h *handler) signup(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()

	session, _ := sessions.Store.Get(request, sessions.UserTokenStoreKey)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderSignup(writer, request)

		return
	}

	email := request.FormValue("email")
	password := request.FormValue("password")
	passwordConfirmation := request.FormValue("passwordConfirmation")

	response, err := h.api.Signup(email, password, passwordConfirmation)

	if err != nil {
		session.AddFlash(err.Error())

		err = session.Save(request, writer)

		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		h.renderSignup(writer, request)

		return
	}

	session.Values["token"] = response.Token
	session.Values["user"] = response.User

	err = session.Save(request, writer)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	request.Form.Del("email")
	request.Form.Del("password")
	request.Form.Del("passwordConfirmation")

	http.Redirect(writer, request, "/profile/edit", http.StatusTemporaryRedirect)
}
