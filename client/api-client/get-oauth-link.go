package api_client

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/auth"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type oauthResult struct {
	auth.OAuthResult

	OAuthState *http.Cookie
}

func (c *Client) GetOAuthLink() *oauthResult {
	var result oauthResult

	response, err := c.http.Post(c.domain+"/auth/google/login", "application/json", nil)

	if err != nil {
		return nil
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil
	}

	err = json.Unmarshal(bodyBytes, &result)

	if err != nil {
		return nil
	}

	result.OAuthState = response.Cookies()[0]

	return &result
}
