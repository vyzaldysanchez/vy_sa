package api_client

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/users"
	"bitbucket.org/vyzaldysanchez/user-management/client/utils"
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
)

func (c *Client) EditProfile(userId uint, payload *users.User, authToken string) error {
	jsonValue, err := json.Marshal(payload)

	if err != nil {
		return err
	}

	userIdStr := strconv.Itoa(int(userId))

	url := c.domain + "/users/" + userIdStr + "?token=" + authToken

	response, err := c.http.Post(url, "application/json", bytes.NewBuffer(jsonValue))

	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusOK {
		return errors.New(utils.GetErrorMessage(response))
	}

	return nil
}
