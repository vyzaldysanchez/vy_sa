package api_client

func (c *Client) Logout(authToken string) error {
	_, err := c.http.Post(c.domain+"/auth/logout?token="+authToken, "application/json", nil)

	if err != nil {
		return err
	}

	return nil
}
