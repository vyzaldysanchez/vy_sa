package api_client

import (
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"
)

type Client struct {
	domain string
	http   *http.Client
}

func (c *Client) SetCookie(url *url.URL, cookie *http.Cookie) {
	c.http.Jar.SetCookies(url, []*http.Cookie{cookie})
}

func New(domain string) *Client {
	jar, _ := cookiejar.New(nil)

	httpClient := &http.Client{
		Jar:     jar,
		Timeout: 15 * time.Second,
	}

	return &Client{domain: domain + "/api", http: httpClient}
}
