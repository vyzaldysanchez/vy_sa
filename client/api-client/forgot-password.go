package api_client

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/passwords"
	"bitbucket.org/vyzaldysanchez/user-management/client/utils"
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
)

func (c *Client) ForgotPassword(email string) error {
	request := passwords.ForgotPasswordRequest{
		Email: email,
	}

	jsonValue, err := json.Marshal(request)

	if err != nil {
		return err
	}

	response, err := c.http.Post(c.domain+"/passwords/forgot", "application/json", bytes.NewBuffer(jsonValue))

	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusOK {
		return errors.New(utils.GetErrorMessage(response))
	}

	return nil
}
