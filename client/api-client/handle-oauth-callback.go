package api_client

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/auth"
	"bitbucket.org/vyzaldysanchez/user-management/client/utils"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func (c *Client) HandleOauthCallback(query string) (*auth.LoginResult, error) {
	result := new(auth.LoginResult)

	response, err := c.http.Get(c.domain + "/auth/google/callback?" + query)

	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, errors.New(utils.GetErrorMessage(response))
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bodyBytes, result)

	if err != nil {
		return nil, err
	}

	return result, nil
}
