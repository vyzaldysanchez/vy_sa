package api_client

import (
	"bitbucket.org/vyzaldysanchez/user-management/api/auth"
	"bitbucket.org/vyzaldysanchez/user-management/client/utils"
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func (c *Client) Login(email, password string) (*auth.LoginResult, error) {
	loginResult := new(auth.LoginResult)

	credentials := auth.LoginCredentials{
		Email:    email,
		Password: password,
	}

	jsonValue, err := json.Marshal(credentials)

	if err != nil {
		return nil, err
	}

	response, err := c.http.Post(c.domain+"/auth/login", "application/json", bytes.NewBuffer(jsonValue))

	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, errors.New(utils.GetErrorMessage(response))
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bodyBytes, loginResult)

	return loginResult, nil
}
