package utils

import (
	"io/ioutil"
	"net/http"
)

func GetErrorMessage(response *http.Response) string {
	data, _ := ioutil.ReadAll(response.Body)

	return string(data)
}
