module bitbucket.org/vyzaldysanchez/user-management

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
	github.com/jinzhu/gorm v1.9.15
	github.com/joho/godotenv v1.3.0
	github.com/mailgun/mailgun-go/v4 v4.1.3
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
