# cloud.google.com/go v0.34.0
cloud.google.com/go/compute/metadata
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/go-chi/chi v4.0.0+incompatible
github.com/go-chi/chi
# github.com/go-sql-driver/mysql v1.5.0
## explicit
github.com/go-sql-driver/mysql
# github.com/golang/protobuf v1.2.0
github.com/golang/protobuf/proto
# github.com/gorilla/handlers v1.4.2
## explicit
github.com/gorilla/handlers
# github.com/gorilla/mux v1.7.4
## explicit
github.com/gorilla/mux
# github.com/gorilla/securecookie v1.1.1
github.com/gorilla/securecookie
# github.com/gorilla/sessions v1.2.0
## explicit
github.com/gorilla/sessions
# github.com/jinzhu/gorm v1.9.15
## explicit
github.com/jinzhu/gorm
# github.com/jinzhu/inflection v1.0.0
github.com/jinzhu/inflection
# github.com/joho/godotenv v1.3.0
## explicit
github.com/joho/godotenv
# github.com/mailgun/mailgun-go/v4 v4.1.3
## explicit
github.com/mailgun/mailgun-go/v4
github.com/mailgun/mailgun-go/v4/events
# github.com/mailru/easyjson v0.7.0
github.com/mailru/easyjson
github.com/mailru/easyjson/buffer
github.com/mailru/easyjson/jlexer
github.com/mailru/easyjson/jwriter
# github.com/pkg/errors v0.8.1
github.com/pkg/errors
# golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
## explicit
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
# golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
golang.org/x/net/context
golang.org/x/net/context/ctxhttp
# golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
## explicit
golang.org/x/oauth2
golang.org/x/oauth2/google
golang.org/x/oauth2/internal
golang.org/x/oauth2/jws
golang.org/x/oauth2/jwt
# google.golang.org/appengine v1.4.0
google.golang.org/appengine
google.golang.org/appengine/internal
google.golang.org/appengine/internal/app_identity
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/modules
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
