package main

import (
	"bitbucket.org/vyzaldysanchez/user-management/api"
	"bitbucket.org/vyzaldysanchez/user-management/api/database"
	"bitbucket.org/vyzaldysanchez/user-management/client"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		// We are running the server in an environment like Heroku in which we set the env vars in some other way
		fmt.Println(fmt.Errorf("couldn't load env variables file: %v", err.Error()))
	}

	db, err := database.ConnectDB()
	defer db.Close()

	if err != nil {
		log.Fatal(err.Error())

		return
	}

	router := mux.NewRouter().StrictSlash(true)

	apiHandler := api.Start(db, router)
	clientHandler := client.Start(router)

	port := os.Getenv("PORT")

	if port == "" {
		port = "8000"
	}

	handler := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		subRoute := strings.Split(request.RequestURI, "/")[1]

		if subRoute == "api" {
			apiHandler.ServeHTTP(writer, request)
			return
		}

		clientHandler.ServeHTTP(writer, request)
	})

	log.Fatal(http.ListenAndServe(":"+port, handler))
}
